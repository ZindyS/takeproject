package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

//Кастомный адаптер для RecView четвёртого типа
public class FourthTypeRecViewAdapter extends RecyclerView.Adapter<FourthTypeRecViewAdapter.Vh> {

    String[] item_names = {"Электроника", "Компьютерная техника", "Бытовая техника", "Товары для ремонта", "Стирка и уборка", "Товары для дома", "Дача, сезонные товары", "Детские товары", "Продукты", "Красота", "Здоровье", "Товары для животных", "Спорт и отдых"};
    Context context;

    @NonNull
    @Override
    public Vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new Vh(LayoutInflater.from(parent.getContext()).inflate(R.layout.fourthtype_recview_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final Vh holder, final int position) {
        //Редактируем элемент
        holder.text.setText(item_names[position]);
        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Вы нажали на \"" + item_names[position] + "\"", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return item_names.length;
    }

    public class Vh extends RecyclerView.ViewHolder {
        TextView text;
        LinearLayout lin;
        public Vh(@NonNull View itemView) {
            //Айдишки
            super(itemView);
            text = itemView.findViewById(R.id.nameofshit);
            lin = itemView.findViewById(R.id.lin);
        }
    }
}
