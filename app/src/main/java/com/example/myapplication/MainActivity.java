package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

//Главный класс
public class MainActivity extends AppCompatActivity {

    EditText text;
    String[] names = {"Стоит приглядеться", "А это интересно", " ", "Каталог товаров", "Как для меня выбрано"};
    int[] types = {0, 1, 2, 3, 4};
    ArrayList<String[]> cash = new ArrayList<String[]>(), item_names = new ArrayList<String[]>();
    ArrayList<int[]> stars = new ArrayList<int[]>(), some = new ArrayList<int[]>();
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Записываем значения в list'ы
        cash.add(new String[]{"17₽", "154₽", "199₽", "25₽", "1899₽"});
        cash.add(new String[]{"17₽", "154₽", "199₽", "25₽", "1899₽"});
        cash.add(new String[]{"17₽", "154₽", "199₽", "25₽", "1899₽"});
        cash.add(new String[]{" "});
        cash.add(new String[]{"17₽", "154₽", "199₽", "25₽", "1899₽"});
        item_names.add(new String[]{"Бетономешалка из бетона", "Тостер-хреностер модели i500", "Кружка с путиным", "Пластмассовая ложка AirBender3000", "Зарплата Россиян"});
        item_names.add(new String[]{"Бетономешалка из бетона", "Тостер-хреностер модели i500", "Кружка с путиным", "Пластмассовая ложка AirBender3000", "Зарплата Россиян"});
        item_names.add(new String[]{"Бетономешалка из бетона", "Тостер-хреностер модели i500", "Кружка с путиным", "Пластмассовая ложка AirBender3000", "Зарплата Россиян"});
        item_names.add(new String[]{" "});
        item_names.add(new String[]{"Бетономешалка из бетона", "Тостер-хреностер модели i500", "Кружка с путиным", "Пластмассовая ложка AirBender3000", "Зарплата Россиян"});
        stars.add(new int[]{5, 2, 5, 4, 0});
        stars.add(new int[]{5, 2, 5, 4, 0});
        stars.add(new int[]{5, 2, 5, 4, 0});
        stars.add(new int[]{0});
        stars.add(new int[]{5, 2, 5, 4, 0});
        some.add(new int[]{1, 18, 999, 26, 999});
        some.add(new int[]{1, 18, 999, 26, 999});
        some.add(new int[]{1, 18, 999, 26, 999});
        some.add(new int[]{0});
        some.add(new int[]{1, 18, 999, 26, 999});

        //Айдишки
        text = findViewById(R.id.editText);
        recyclerView = findViewById(R.id.recyclerView);

        //Настраиваем главный RecView
        FirstRecViewAdapter adapter = new FirstRecViewAdapter(names, types, cash, item_names, stars, some, this);
        recyclerView.setAdapter(adapter);

        //Убираем клавиатуру
        text.clearFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    //При нажатии на каталог
    public void onBruhClicked(View v) {
        Toast.makeText(this, "Вы нажали на католог", Toast.LENGTH_SHORT).show();
    }

    //При нажатии на скидки
    public void onHurbClicked(View v) {
        Toast.makeText(this, "Вы нажали на скидки", Toast.LENGTH_SHORT).show();
    }
}
