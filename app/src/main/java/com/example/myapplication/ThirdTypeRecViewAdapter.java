package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

//Кастомный адаптер для RecView третьего типа
public class ThirdTypeRecViewAdapter extends RecyclerView.Adapter<ThirdTypeRecViewAdapter.Vh> {

    String[] item_names, cash, oldcash= {"30₽", "199₽", "249₽", "39₽", "1999₽"};
    int[] stars, some;
    Context context;
    int[] images = {R.drawable.i1, R.drawable.i2, R.drawable.i3, R.drawable.i4, R.drawable.i5, R.drawable.i6, R.drawable.i7, R.drawable.i8, R.drawable.i9, R.drawable.i10};

    //Кастомный конструктор
    public ThirdTypeRecViewAdapter(String[] item_names, int[] stars, String[] cash, int[] hurb) {
        this.item_names = item_names;
        this.stars = stars;
        this.some = hurb;
        this.cash = cash;
    }

    @NonNull
    @Override
    public Vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Vh(LayoutInflater.from(parent.getContext()).inflate(R.layout.thirdtype_recview_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Vh holder, int position) {
        //Редактирование элемента
        holder.text.setText(item_names[position]);
        holder.somestars.setText(String.valueOf(some[position]) + " отзывов");
        holder.cash.setText(cash[position]);
        holder.imageView.setImageResource(images[position]);
        holder.oldcash.setText(oldcash[position]);
    }

    @Override
    public int getItemCount() {
        return item_names.length;
    }

    public class Vh extends RecyclerView.ViewHolder {
        TextView text, somestars, cash, oldcash;
        ImageView imageView, star1, star2, star3, star4, star5;

        public Vh(@NonNull View itemView) {
            super(itemView);
            //Айдишки
            text = itemView.findViewById(R.id.nameofitem);
            somestars = itemView.findViewById(R.id.somestars);
            imageView = itemView.findViewById(R.id.imageView4);
            cash = itemView.findViewById(R.id.cash);
            oldcash = itemView.findViewById(R.id.oldcash);
            star1 = itemView.findViewById(R.id.star1);
            star2 = itemView.findViewById(R.id.star2);
            star3 = itemView.findViewById(R.id.star3);
            star4 = itemView.findViewById(R.id.star4);
            star5 = itemView.findViewById(R.id.star5);
        }
    }
}
