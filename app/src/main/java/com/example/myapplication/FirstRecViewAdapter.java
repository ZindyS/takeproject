package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

//Кастомный адаптер для главного RecView
public class FirstRecViewAdapter extends RecyclerView.Adapter<FirstRecViewAdapter.VH> {

    String[] names;
    int[] type;
    ArrayList<String[]> cash;
    ArrayList<String[]> item_names;
    ArrayList<int[]> stars;
    ArrayList<int[]> some;
    Context context;

    //Кастомный конструктор
    public FirstRecViewAdapter (String[] names, int[] type, ArrayList<String[]> cash, ArrayList<String[]> item_names, ArrayList<int[]> stars, ArrayList<int[]> some, Context context) {
        this.names = names;
        this.type = type;
        this.cash = cash;
        this.item_names = item_names;
        this.stars = stars;
        this.some = some;
        this.context = context;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Создаём новый объект типа VH
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.first_recview_item, parent, false));
    }

    //Запрашиваем кол-во элементов
    @Override
    public int getItemCount() {
        return names.length;
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        //Проверка на тип
        if (type[position]==0) {
            //Настройка элемента
            holder.name.setText(names[position]);
            //Настройка нового RecView
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            holder.recyclerView.setLayoutManager(linearLayoutManager);
            FirstTypeRecViewAdapter adapter = new FirstTypeRecViewAdapter(item_names.get(position), stars.get(position), cash.get(position), some.get(position));
            holder.recyclerView.setAdapter(adapter);
        } else if (type[position]==1) {
            //Настройка элемента
            holder.name.setText(names[position]);
            //Настройка нового RecView
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            holder.recyclerView.setLayoutManager(linearLayoutManager);
            SecondTypeRecViewAdapter adapter = new SecondTypeRecViewAdapter(item_names.get(position), stars.get(position), cash.get(position), some.get(position));
            holder.recyclerView.setAdapter(adapter);
        } else  if (type[position] == 2) {
            //Настройка элемента
            holder.name.setVisibility(View.INVISIBLE);
            holder.sis.setVisibility(View.INVISIBLE);
            //Настройка нового RecView
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            holder.recyclerView.setLayoutManager(linearLayoutManager);
            ThirdTypeRecViewAdapter adapter = new ThirdTypeRecViewAdapter(item_names.get(position), stars.get(position), cash.get(position), some.get(position));
            holder.recyclerView.setAdapter(adapter);
        } else if (type[position] == 3) {
            //Настройка элемента
            holder.name.setText(names[position]);
            //Настройка нового RecView
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            holder.recyclerView.setLayoutManager(linearLayoutManager);
            FourthTypeRecViewAdapter adapter = new FourthTypeRecViewAdapter();
            holder.recyclerView.setAdapter(adapter);
        } else {
            holder.name.setText(names[position]);
            holder.sis.setVisibility(View.INVISIBLE);

        }
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView name;
        View sis;
        RecyclerView recyclerView;
        public VH(@NonNull View itemView) {
            super(itemView);
            //Айдишки
            name = itemView.findViewById(R.id.nameofgroup);
            sis = itemView.findViewById(R.id.view);
            recyclerView = itemView.findViewById(R.id.thisRecView);
        }
    }
}
