package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

//Класс Slash Screen'а
public class SplashScreen extends AppCompatActivity {

    ConstraintLayout con;
    Animation anim;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //Айдишки
        con = findViewById(R.id.con);

        //Пытаемся сделать анимацию (не работает)
        anim = AnimationUtils.loadAnimation(this, R.anim.splashanim);
        anim.setDuration(500);

        final Intent intent = new Intent(SplashScreen.this, MainActivity.class);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                con.setAnimation(anim);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(intent);
                        finish();
                    }
                }, 500);
            }
        }, 1000);
    }
}
